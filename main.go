package main

import (
	"encoding/json"
	"fmt"
	"gopkg.in/telegram-bot-api.v4"
	"io/ioutil"
	"log"
	"os"
	"strings"
)

type Question struct {
	Question string
	Answers  []struct {
		Answer string
		Color  string
		Type   string
	}
}

var questions []Question

type UserContext struct {
	ChatID   int64
	UserData struct {
		FirstName  string
		LastName   string
		MiddleName string
		Negative   int64
		Positive   int64
		Red        int64
		Blue       int64
		Yellow     int64
		White      int64
		test_q     bool
		q_num      int64
	}
}

func ParseJsonFile(filePath string) (err error) {

	file, err1 := ioutil.ReadFile(filePath)
	if err1 != nil {
		fmt.Printf("// Не возможно прочитать фаил %s\n", filePath)
		fmt.Printf("Ошибка файла: %v\n", err1)
		os.Exit(1)
	}
	err2 := json.Unmarshal(file, &questions)
	if err2 != nil {
		fmt.Println("Ошибка:", err2)
		os.Exit(1)
	}

	return err2
}

func main() {

	us := UserContext{}
	result := make(map[string]int64)
	bot, _ := tgbotapi.NewBotAPI(os.Getenv("token"))
	bot.Debug = false

	log.Printf("Запущен бот -  %s", bot.Self.UserName)

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates, _ := bot.GetUpdatesChan(u)

	filePath := "questions.json"
	ParseJsonFile(filePath)

	for update := range updates {
		msg := tgbotapi.NewMessage(update.Message.Chat.ID, update.Message.Text)
		command := strings.Split(update.Message.Text, " ")[0]

		//fmt.Println(command)

		if update.Message == nil {
			Keyboard_page1 := tgbotapi.NewReplyKeyboard(
				tgbotapi.NewKeyboardButtonRow(
					tgbotapi.NewKeyboardButton("start"),
				))

			msg.ReplyMarkup = Keyboard_page1
			bot.Send(msg)
		}
		//
		us.ChatID = update.Message.Chat.ID
		//	log.Printf("[%s] %s", update.Message.From.UserName, update.Message.Text)

		switch command {
		case "/start", "start":

			result = make(map[string]int64)
			us.UserData.q_num = 0
			msg.Text = "Пожалуйста введите свое  ФИО для начала теста\n" +
				"Например `/reg Сидоров Иван Иванович`"
			bot.Send(msg)

		case "/reg":

			message := strings.Split(update.Message.Text, " ")[1:]
			//fmt.Println(message)

			if len(message) == 3 {
				msg.Text = fmt.Sprintf("Ваша фамилия: %s\nВаше имя: %s\nВашe отчество: %s\n",
					message[0], message[1], message[2])

				us.UserData.FirstName = message[0]
				us.UserData.LastName = message[1]
				us.UserData.MiddleName = message[2]

				bot.Send(msg)
				us.UserData.test_q = true

			} else {
				msg.Text = "Данные введены не верно!\nПовторите ввод!"
				bot.Send(msg)

			}

		default:
		}
		if us.UserData.test_q {
			if us.UserData.q_num > 44 {

				us.UserData.Blue = result["blue"]
				us.UserData.Red = result["red"]
				us.UserData.White = result["white"]
				us.UserData.Yellow = result["yellow"]
				us.UserData.Negative = result["negative"]
				us.UserData.Positive = result["positive"]
				msg.Text = fmt.Sprint("Тест Окончен, спасибо за участие\n"+
					"Статистика:\n"+
					"Красный: ", int(us.UserData.Red), "\n"+
					"Синий: ", int(us.UserData.Blue), "\n"+
					"Белый: ", int(us.UserData.White), "\n"+
					"Желтый: ", int(us.UserData.Yellow), "\n"+
					"Негатив: ", int(us.UserData.Negative), "\n"+
					"Позитив: ", int(us.UserData.Positive), "\n")
				msg.ReplyMarkup = tgbotapi.NewReplyKeyboard(
					tgbotapi.NewKeyboardButtonRow(
						tgbotapi.NewKeyboardButton(""),
					))
				log.Println(us.UserData.Blue, ">", us.UserData.Red, ">", us.UserData.White, ">", us.UserData.Yellow, ">", us.UserData.Negative, ">", us.UserData.Positive)
				bot.Send(msg)

				us.UserData.test_q = false

			} else {
				msg.Text = questions[us.UserData.q_num].Question

				msg.ReplyMarkup = tgbotapi.NewReplyKeyboard(
					tgbotapi.NewKeyboardButtonRow(
						tgbotapi.NewKeyboardButton(questions[us.UserData.q_num].Answers[0].Answer),
					),
					tgbotapi.NewKeyboardButtonRow(
						tgbotapi.NewKeyboardButton(questions[us.UserData.q_num].Answers[1].Answer),
					),
					tgbotapi.NewKeyboardButtonRow(
						tgbotapi.NewKeyboardButton(questions[us.UserData.q_num].Answers[2].Answer),
					),
					tgbotapi.NewKeyboardButtonRow(
						tgbotapi.NewKeyboardButton(questions[us.UserData.q_num].Answers[3].Answer),
					))

				bot.Send(msg)
				us.UserData.q_num += 1
			}
			for _, j := range questions {

				for _, n := range j.Answers {
					if n.Answer == update.Message.Text {
						result[n.Color] += 1
						fmt.Println(update.Message.Text, ">", n.Color, "+1")
						result[n.Type] += 1
						fmt.Println(update.Message.Text, ">", n.Type, "+1")

					}

				}
			}
		}

	}
}
