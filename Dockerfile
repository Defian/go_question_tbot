FROM golang:1.9
ADD . /go/src/tbot
ENV token=""
WORKDIR /go/src/tbot
RUN go install
RUN go build
CMD /go/src/tbot/tbot
